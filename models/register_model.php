<?php

class Register_model extends Model{
	
	function __construct(){
		
	}	

	/**
	 * Lisab andmebaasi uue kasutaja ja tema parooli
	 * @param type $username kasutajanimi
	 * @param type $password parool
	 * @return type tõeväärtus, kas kasutaja võib lisada andmebaasi
	 */
	function add_user($username, $password){
		$conn=$this->connect_db();

		$hash = password_hash($password, PASSWORD_DEFAULT);
		$query = 'INSERT INTO mpaakspu_users (username, password) VALUES (?, ?)';
		$stmt = mysqli_prepare($conn, $query);
		if (mysqli_error($conn)) {
			echo mysqli_error($conn);
			exit;
		}
		mysqli_stmt_bind_param($stmt, 'ss', $username, $hash);
		mysqli_stmt_execute($stmt);
		$id = mysqli_stmt_insert_id($stmt);
		mysqli_stmt_close($stmt);
		return $id;
	}

	/**
	 * Kontrollib, kas kasutajanimi on juba kasutusel
	 * @param type $username kasutajanimi
	 * @return type kasutajanime andmebaasis mitte ilmnemise tõeväärtus
	 */
	function checkName($username){
		$conn=$this->connect_db();
		$query = 'SELECT username FROM mpaakspu_users WHERE username=? LIMIT 1';
		$stmt = mysqli_prepare($conn, $query);
		if (mysqli_error($conn)) {
			echo mysqli_error($conn);
			exit;
		}

		mysqli_stmt_bind_param($stmt, 's', $username);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $user);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		if($user==$username){
			//echo 'kasutusel';
			return false;
		}
		return true;

	}

}


?>