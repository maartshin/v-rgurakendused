<?php

class Auctions_model extends Model{
	function __construct(){

	}	

	//Tagastab käimasolevad oksjonid
	function show_auctions(){
		$conn=$this->connect_db();

		$dt=new DateTime();
		$current_dt=$dt->format("Y-m-d H:i:s");
		$query = 'SELECT * FROM mpaakspu_auctions WHERE end_time>"'.$current_dt.'"';
		$result = mysqli_query($conn, $query);
		$rows = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row;
		}
		return $rows;
	}
	
	//Tagastab lõppenud oksjonid
	function show_ended_auctions(){
		$conn=$this->connect_db();

		$dt=new DateTime();
		$current_dt=$dt->format("Y-m-d H:i:s");
		$query = 'SELECT * FROM mpaakspu_auctions WHERE end_time<="'.$current_dt.'"';
		$result = mysqli_query($conn, $query);
		$rows = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row;
		}
		return $rows;
	}	

	/**
	 * Tagastab oksjoni tulemused
	 * @param type $id oksjoni id
	 * @return type oksjoni informatsioon
	 */
	function show_results($id){
		$conn=$this->connect_db();
		$query="SELECT * FROM mpaakspu_bids WHERE auction_id=".$id;
		$result = mysqli_query($conn, $query);
		$rows = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row;
		}
		return $rows;
	}

	/**
	 * Lisab pakkumiste tabelisse uue pakkumise
	 * @param type $id oksjoni id
	 * @param type $bid pakkumine
	 * @param type $time pakkumise aeg
	 * @return type
	 */
	function bid($id, $bid, $time){
		$conn=$this->connect_db();
		$username=Session::get('name');

		$query='INSERT INTO mpaakspu_bids (auction_id,username,bid,bid_time) VALUES(?, ?, ?, ?)';
		$stmt = mysqli_prepare($conn, $query);
		mysqli_stmt_bind_param($stmt, 'ssss', $id, $username, $bid, $time);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
		$this->update_auctions($id,$bid,$username);

	}

	/**
	 * Uuendab oksjonite tabelis parimat pakkumist
	 * @param type $id oksjoni id
	 * @param type $bid pakkumine
	 * @param type $username kasutajanimi
	 * @return type
	 */
	function update_auctions($id, $bid, $username){
		$conn=$this->connect_db();
		$query="UPDATE mpaakspu_auctions SET bid='".$bid."',last_bidder='".$username."' WHERE auction_id=".$id;
		$stmt = mysqli_prepare($conn, $query);
		mysqli_stmt_execute($stmt);
		$id = mysqli_stmt_insert_id($stmt);
		mysqli_stmt_close($stmt);
	}

	/**
	 * Tagastab pakkumise andmed
	 * @param type $id oksjoni id
	 * @return type pakkumise andmed
	 */
	function show_bid($id){
		$conn=$this->connect_db();
		$query="SELECT * FROM mpaakspu_auctions WHERE auction_id=".$id;
		$result = mysqli_query($conn, $query);
		$row = mysqli_fetch_assoc($result);
		return $row;
	}

}

?>