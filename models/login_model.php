<?php

class Login_model extends Model{
	function __construct(){
		
	}	

	/**
	 * Kontrollib kasutajanime ja parooli
	 * @param type $username kasutajanimi
	 * @param type $password parool
	 * @return type kasutaja id ja nimi
	 */
	function get_user($username,$password){
		$conn=$this->connect_db();
		$query = 'SELECT user_id, password FROM mpaakspu_users WHERE username=? LIMIT 1';
		$stmt = mysqli_prepare($conn, $query);
		if (mysqli_error($conn)) {
			echo mysqli_error($conn);
			exit;
		}

		mysqli_stmt_bind_param($stmt, 's', $username);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $id, $hash);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		if(password_verify($password, $hash)){
			$login=array($id, $username);
		}
		else{
			$login=false;
		}
		return $login;

	}

}


?>