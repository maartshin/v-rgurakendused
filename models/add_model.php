<?php

class Add_model extends Model{
	function __construct(){

	}	

	/**
	 * Lisab andmed auctions andmebaasi.
	 * @param type $name oksjoni nimi
	 * @param type $bid alghind
	 * @param type $time oksjoni loomise aeg
	 * @return type
	 */
	function add($name, $bid, $time){
		$conn=$this->connect_db();
		$last_bidder=Session::get('name');
		$query = 'INSERT INTO mpaakspu_auctions (item_name, bid, end_time, last_bidder) VALUES (?, ?, ?, ?)';
		$stmt = mysqli_prepare($conn, $query);
		mysqli_stmt_bind_param($stmt, 'ssss', $name, $bid, $time, $last_bidder);
		mysqli_stmt_execute($stmt);
		$id = mysqli_stmt_insert_id($stmt);
		mysqli_stmt_close($stmt);
		$this->update_bids($id, $bid);
		header('Location:'.URL.'auctions');

	}

	/**
	 * Lisab pakkumiste tabelisse uue pakkumise
	 * @param type $auction_id oksjoni id
	 * @param type $time aeg, mil pakkumine tehti
	 * @param type $bid pakkumise väärtus
	 * @return type
	 */
	function update_bids($auction_id, $bid){
		$conn=$this->connect_db();
		$last_bidder='alghind';
		$dt=new DateTime();
		$time=$dt->format("Y-m-d H:i:s");
		$query='INSERT INTO mpaakspu_bids (auction_id,username,bid,bid_time) VALUES(?, ?, ?, ?)';
		$stmt = mysqli_prepare($conn, $query);
		mysqli_stmt_bind_param($stmt, 'ssss', $auction_id, $last_bidder, $bid, $time);
		mysqli_stmt_execute($stmt);
		$id = mysqli_stmt_insert_id($stmt);
		mysqli_stmt_close($stmt);
	}
}

?>