<div class="container content">
	<table class="table"><thead>
		<tr>
			<th>Pakkumine</th>
			<th>Pakkuja</th>
			<th>Aeg</th>
		</thead>
	</tr>
	<tbody>
		<?php
		foreach ($this->results as $rida): ?>

		<tr>
			<td>
				<?= htmlspecialchars($rida['bid']); ?> €
			</td>
			<td>
				<?= htmlspecialchars($rida['username']); ?>
			</td>
			<td id="time">
				<?= htmlspecialchars($rida['bid_time']); ?>
			</td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
</div>