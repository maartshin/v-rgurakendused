
<div class="container content" id="uued">
	<p id="show_ended">
		<button class="btn btn-default nupp" type="button">Kuva lõppenud</button>
	</p>
	<table class="table">
		<thead>
			<tr>
				<th>
					Nimi
				</th>
				<th>
					Viimane pakkumine
				</th>
				<th>
					Aega jäänud
				</th>
				<th>
					Tegevused
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($this->data1 as $rida): ?>

			<tr>
				<td>
					<?= htmlspecialchars($rida['item_name']); ?>
				</td>
				<td>
					<?= htmlspecialchars($rida['bid']); ?> €
				</td>
				<td id="time">
					<?= htmlspecialchars(Auctions::timeleft($rida['end_time'])); ?>
				</td>
				<td>
					<form action="<?php echo URL2;?>index.php" method="get">
						<input type='hidden' name='url' value='auctions'>	
						<input type='hidden' name='view' value='bid'>	
						<input type='hidden' name='id' value=<?= $rida['auction_id'] ;?> >
						<button class="btn btn-default nupp" type='submit'>Mine pakkuma</button>


					</form>
				</td>
			</tr>

		<?php endforeach; ?>

	</tbody>
</table>
</div>
<div class="container content" id="vanad" style="display:none">
<p id="show_new">
		<button class="btn btn-default nupp" type="button">Kuva uued</button>
	</p>
<table class="table">

	<thead>
		<tr>
			<th>
				Nimi
			</th>
			<th>
				Viimane pakkumine
			</th>
			<th>
				Lõpuaeg
			</th>
			<th>
				Tegevused
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($this->data2 as $rida): ?>

		<tr>
			<td>
				<?= htmlspecialchars($rida['item_name']); ?>
			</td>
			<td>
				<?= htmlspecialchars($rida['bid']); ?> €
			</td>
			<td>
				<?= htmlspecialchars($rida['end_time']); ?>
			</td>
			<td>
				<form action="<?php echo URL2;?>index.php" method='GET'>
					<input type='hidden' name='url' value='auctions'>	
					<input type='hidden' name='view' value='results' >
					<input type='hidden' name='id' value=<?= $rida['auction_id'] ;?> >
					<button class="btn btn-default nupp" type='submit'>Vaata tulemust</button>


				</form>
			</td>
		</tr>

	<?php endforeach; ?>

</tbody>
</table>

</div>