<div class="container content">
	<form action="<?php echo URL;?>auctions&view=bid&id=<?php echo $_GET['id']; ?>" method="post">
		<input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
		<table class="table">
			<thead>
				<tr>
					<th>Nimi</th>
					<td><?= htmlspecialchars($this->show_bid['item_name']); ?></td>
				</tr>
			</thead>
			<tbody>
				<input type="hidden" name="last_bid" value="<?php echo $this->show_bid['bid']; ?>">
				<input type="hidden" name="auction_id" id="auction_id" value=<?= $this->show_bid['auction_id']; ?>>
				<input type="hidden" name="end_time" value="<?php echo $this->show_bid['end_time']; ?>">
				<tr>
					<th>Parim pakkumine</th>
					<td><?= htmlspecialchars($this->show_bid['bid']); ?> €</td>
				</tr>
				<tr>
					<th>Viimane pakkuja</th>
					<td><?= htmlspecialchars($this->show_bid['last_bidder']); ?></td>
				</tr>
				<tr>
					<th>Aega jäänud</th>
					<td id="time"><?= Auctions::timeleft($this->show_bid['end_time']); ?></td>
				</tr>
				<tr>
					<th>Tee pakkumine</th>
					<td>
						<div class="col-xs-6 nopadding">
							<input type="number" class="form-control" name="bid" step="0.01" min="<?= $this->show_bid['bid']+0.01; ?>">
						</div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><button class="btn btn-default nupp" type="submit">Paku</button></td>
				</tr>
			</tbody>
		</table>
	</form>

	<?php foreach ($this->messages as $message):?>
		<div class="alert alert-danger">
			<?= $message; ?>
		</div>
	<?php endforeach; ?>
	
</div>