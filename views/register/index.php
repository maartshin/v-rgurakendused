<div class="container">
	<form id="register" action="<?php echo URL; ?>register" method="POST">
		<input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
		<div class="form-group">
			<label for="username">Username</label>
			<input name="username" type="text" maxlength="12" class="form-control" id="username" placeholder="Username">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input name="password" type="password" maxlength="12" class="form-control" id="password" placeholder="Password">
		</div>
		<div class="form-group">
			<label for="password2">Confirm password</label>
			<input name="password2" type="password" maxlength="12" class="form-control" id="password2" placeholder="Re-type password">
		</div>
		<button type="submit" class="btn btn-primary">Register</button>
	</form>
	<br/>

	<?php foreach ($this->messages as $message):?>
		<div class="alert alert-danger">
			<?= $message; ?>
		</div>
	<?php endforeach; ?>
</div>