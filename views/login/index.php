<div class="container">
	<form id="login" action="<?php echo URL; ?>login" method="POST">
		<input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
		<div class="form-group">
			<label for="username">Username</label>
			<input name="username" type="text" maxlength="12" class="form-control" id="username" placeholder="Username">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input name="password" type="password" maxlength="12" class="form-control" id="password" placeholder="Password">
		</div>
		<button type="submit" class="btn btn-primary">Login</button>
	</form>

	<br/>

	<?php foreach ($this->messages as $message):?>
		<div class="alert alert-danger">
			<?= $message; ?>
		</div>
	<?php endforeach; ?>
</div>