<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $this->title; ?></title>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=320, height=device-height, target-densitydpi=medium-dpi" />
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<script type="text/javascript" src="<?php echo URL2?>public/js/jquery-1.12.3.min.js"></script>
	<link rel="stylesheet" href="<?php echo URL2?>public/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo URL2?>public/css/default.css">
	<script src="<?php echo URL2?>public/js/bootstrap.min.js"></script>
	<?php 
	if (isset($this->js)) 
	{
		if(!isset($_GET['view'])){
			echo '<script type="text/javascript" src="'.URL2.'views/'.$this->js.'"></script>';
		}
	}
	?>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="<?php echo URL;?>index">E-oksjon</a> </div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li <?php if($this->active=='index'){ echo 'class="active"'; }?>><a href="<?php echo URL?>index">Avaleht</a></li>
						<li <?php if($this->active=='auctions'){ echo 'class="active"'; }?>><a href="<?php echo URL?>auctions">Vaata oksjone</a></li>
						<li <?php if($this->active=='add'){ echo 'class="active"'; }?>><a href="<?php echo URL?>add">Loo oksjon</a></li>
					</ul>
					<?php if(Session::get('login')!=false){
						echo '<ul class="nav navbar-nav navbar-right">
						<li><p class="navbar-text">Logged in as: '.Session::get('name').'</p></li>
						<li><a href="'.URL.'auctions/logoff"><span class="glyphicon glyphicon-log-in"></span> Logoff</a></li>
					</ul>';
				}
				else{
					echo '<ul class="nav navbar-nav navbar-right">
					<li><a href="'.URL.'register"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
					<li><a href="'.URL.'login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				</ul>';
			}
			?>

		</div>
	</div>
</nav>

