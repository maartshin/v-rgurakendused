window.onload = function () {
    document.getElementById('add').addEventListener('submit',

        function (event) {
        // loeme tekstikastidest kasutaja sisestatud andmed
        var name = document.getElementById('name').value;
        var bid = document.getElementById('bid').value;
        var datetime = document.getElementById('datetime').value;

        // kontrollime väärtuseid
        if (!name || !bid ||!datetime) {
            alert('Sisestage andmed!');
            // Katkestame tavalise submit tegevuse, vastasel korral navigeeriks brauser mujale
            event.preventDefault();
            return;
        }
    });
};