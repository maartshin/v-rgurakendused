<div class="container content">
  <form id="add" class="form-horizontal" role="form" action="<?php echo URL;?>add" method="POST">
    <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Nimi:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="bid">Alghind:</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" name="start_bid" id="bid" min="0" step="0.01" placeholder="Enter starting bid">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="datetime">Lõpuaeg:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="datetime" id="datetime" placeholder="YYYY-MM-DD hh:mm:ss">
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default nupp">Lisa</button>
      </div>
    </div>
  </form>


 <?php foreach ($this->messages as $message):?>
  <div class="alert alert-danger">
    <?= $message; ?>
  </div>
<?php endforeach; ?>

</div>
