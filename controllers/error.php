<?php

class Error extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
	}

	//Näitab kasutajale veebilehte
	function view(){
		$this->view->title='Not found';
		$this->view->render('error/index');
	}
	
}
?>