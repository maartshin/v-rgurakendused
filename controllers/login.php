<?php

class Login extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
		if(Session::get('login')!=false){
			header('Location:'.URL.'index');
			exit;
		}
		$this->view->js='login/js/default.js';

	}

	//Näitab kasutajale veebilehte ja saadab sinna teated
	function view(){
		$this->view->title='Logi sisse';
		$this->login();
		$this->view->messages=$this->message_list();
		$this->view->render('login/index');
	}

	//Logib kasutaja sisse ja kontrollib andmete sisestust
	function login(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->check_csrf()){
				if(!isset($_POST['username'],$_POST['password'])){
					$error='Sisendid puuduvad';
					$this->message_add($error);
					return false;
				}
				else{
					$username=htmlspecialchars($_POST['username']);
					$password=htmlspecialchars($_POST['password']);

					if ($username == '' || $password == '') {
						$error='Sisestage andmed';
						$this->message_add($error);
						return false;
					}
					$login=$this->model->get_user($username,$password);
					if(!$login){
						$error='Vale kasutajanimi või parool';
						$this->message_add($error);
						return false;
					}

					session_regenerate_id();
					Session::set('login',$login[0]);
					Session::set('name',$login[1]);
					header('Location:'.URL.'auctions');
					return $login;
				}
			}
			else{
				$error='Vigane päring, CSRF token ei vasta oodatule';
				$this->message_add($error);
			}
		}
	}
	
}
?>