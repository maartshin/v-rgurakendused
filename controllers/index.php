<?php

class Index extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
	}

	//Näitab kasutajale veebilehte
	function view(){
		$this->view->title='Avaleht';
		$this->view->active='index';
		$this->view->messages=$this->message_list();
		$this->view->render('index/index');

	}

	
}

?>