<?php

class Add extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
		if(Session::get('login')==false){
			$error='Peate olema sisselogitud';
			$this->message_add($error);
			header('Location:'.URL.'login');
			exit;
		}
		$this->view->js='add/js/default.js';
	}

	//Näitab kasutajale veebilehte ning saadab sinna vajalikud andmed
	function view(){
		$this->view->title='Lisa';
		$this->add();
		$this->view->messages=$this->message_list();
		$this->view->active='add';
		$this->view->render('add/index');

	}

	//Lisab uue oksjoni
	function add(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->check_csrf()){
				if(!isset($_POST['name'],$_POST['start_bid'],$_POST['datetime'])){
					$error='Väärtused puuduvad';
					$this->message_add($error);
				}
				else{
					$name=htmlspecialchars($_POST['name']);
					$bid=htmlspecialchars($_POST['start_bid']);
					$time=htmlspecialchars($_POST['datetime']);

					if($this->checkInsert($name,$bid,$time)){
						$this->model->add($name,$bid,$time);
						header("Location:".URL."auctions");
					}
				}
			}
			else{
				$error='Vigane päring, CSRF token ei vasta oodatule';
				$this->message_add($error);
			}
		}
	}

	/**
	 * Kontrollib kasutaja sisestust
	 * @param type $name kasutajanimi
	 * @param type $bid pakkumine
	 * @param type $time pakkumise tegemise aeg (datetime formaadis)
	 * @return type andmete õigsuse tõeväärtus
	 */
	function checkInsert($name, $bid, $time){


		if(strlen($name)<1||strlen($bid)==0||strlen($time)<1){
			$error='vigased väärtused!';
			$this->message_add($error);
			return false;
		}
		if($this->validateDate($time)==false){
			$error='Kuupäeva sisestamisel tekkis viga.';
			$this->message_add($error);
			return false;
		}

		if($this->validateDate($time)==true){
			$d1=new DateTime(date("Y-m-d H:i:s"));
			$d2=new DateTime($time);

			if($d1>=$d2){
				$error='Lõpuaeg on möödas.';
				$this->message_add($error);
				return false;
			}
		}
		return true;
	}

	/**
	 * Kontrollib, kas kuupäev on õigesti sisestatud
	 * @param type $date kasutaja poolt sisestatud kuupäev
	 * @return type sobiva kuupäeva vormingu tõeväärtus
	 */
	function validateDate($date){
		$d=DateTime::createFromFormat('Y-m-d H:i:s',$date);
		return $d&&$d->format('Y-m-d H:i:s')===$date;
	}
	
}
?>