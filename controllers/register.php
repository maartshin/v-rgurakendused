<?php

class Register extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
		if(Session::get('login')!=false){
			header('Location:'.URL.'index');
			exit;
		}
		$this->view->js='register/js/default.js';
	}

	//Näitab kasutajale veebilehte
	function view(){
		$this->view->title='Registreerimine';
		$this->registration();
		$this->view->messages=$this->message_list();
		$this->view->render('register/index');

	}

	//Registreerib uue kasutaja ning kontrollib andmete sisestust
	function registration(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->check_csrf()){
				if(!isset($_POST['username'],$_POST['password'],$_POST['password2'])){
					$error= 'Sisendid puuduvad';
					return false;
				}
				else{
					$username=htmlspecialchars($_POST['username']);
					$password=htmlspecialchars($_POST['password']);
					$password2=htmlspecialchars($_POST['password2']);

					if($this->checkInsert($username,$password,$password2)){
						$this->model->add_user($username,$password);
						$message='Kasutaja registreeritud';
						$this->message_add($message);
						header("Location:".URL."login");
						exit;
					}


				}
			}
			else{
				$error='Vigane päring, CSRF token ei vasta oodatule';
				$this->message_add($error);
			}
		}
	}


	/**
	 * Kontrollib kasutaja sisestust
	 * @param type $username kasutajanimi
	 * @param type $password sisestatud parool
	 * @param type $password2 sisestaud parooli kinnitus
	 * @return type andmete õigsuse tõeväärtus
	 */
	function checkInsert($username,$password,$password2){
		if(strlen($username)<7||strlen($password)<7){
			$error= 'Kasutajanimi või parool liiga lühikesed';
			$this->message_add($error);
			return false;
		}
		if(!$this->model->checkName($username)){
			$error= 'Kasutajanimi kasutusel';
			$this->message_add($error);
			return false;
		}
		if($password!=$password2){
			$error= 'Paroolid ei ühti';
			$this->message_add($error);
			return false; 
		}
		return true;
	}


	
}

?>