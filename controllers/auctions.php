<?php

class Auctions extends Controller{
	
	function __construct(){
		parent::__construct();
		Session::init();
		if(Session::get('login')==false){
			$error='Peate olema sisselogitud';
			$this->message_add($error);
			header('Location:'.URL.'login');
			exit;
		}
		$this->view->js='auctions/js/default.js';
	}


	//Näitab kasutajale veebilehte ning saadab sinna vajalikud andmed
	function view(){
		if(isset($_GET['view'])){
			if($_GET['view']=='results'){
				$this->view->title='Tulemused';
				$id=$_GET['id'];
				$this->view->results=$this->model->show_results($id);
				$this->view->render('auctions/results');

			}
			if($_GET['view']=='bid'){
				$this->view->title='Paku';
				$id=$_GET['id'];
				$this->bid();
				$this->view->messages=$this->message_list();
				$this->view->show_bid=$this->model->show_bid($id);
				$this->view->render('auctions/bid');
			}
		}
		else{
			$this->view->title='Vaata';
			$this->view->data1=$this->model->show_auctions();
			$this->view->data2=$this->model->show_ended_auctions();
			$this->view->active='auctions';
			$this->view->render('auctions/index');
		}
	}

	/**
	 * Tagastab, palju on aega järel
	 * @param type $time lõpuaeg (datetime formaadis)
	 * @return type järelejäänud aeg
	 */
	public static function timeleft($time){
		$datetime=strtotime($time);
		$diff=$datetime-time();
		$daysleft=($diff-($diff%(60*60*24)))/(60*60*24);
		$hoursleft=($diff-(($diff-$daysleft*60*60*24)%(60*60))-$daysleft*60*60*24)/(60*60);
		$minutesleft=($diff-$daysleft*60*60*24-$hoursleft*60*60-(($diff-$daysleft*60*60*24-$hoursleft*60*60)%60))/60;
		$secondsleft=$diff-$daysleft*24*60*60-$hoursleft*60*60-$minutesleft*60;
		if($diff<=0){
			$time_left='lõppenud';
		}
		elseif($daysleft>10){
			$time_left= $daysleft.' days ';
		}
		elseif($daysleft>0){
			$time_left= $daysleft.' days '.$hoursleft.' h '.$minutesleft.' min';
		}
		elseif($daysleft==0&&$hoursleft!=0){
			$time_left=$hoursleft.' h '.$minutesleft.' min '.$secondsleft.' sec';
		}
		elseif($hoursleft==0&&$minutesleft!=0){
			$time_left=$minutesleft.' min '.$secondsleft.' sec';
		}
		elseif($hoursleft==0){
			$time_left=$secondsleft.' sec';
		}
		return $time_left;

	}

	//Muudab oksjonite tabelit ning lisab uue pakkumise kirje
	function bid(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->check_csrf()){
				if(!isset($_POST['bid'],$_POST['last_bid'],$_POST['auction_id'], $_POST['end_time'])){
					$error='Vigased väärtused!';
					$this->message_add($error);
					return false;
				}
				else{
					$end_time=htmlspecialchars($_POST['end_time']);
					$last_bid=htmlspecialchars($_POST['last_bid']);
					$bid=htmlspecialchars($_POST['bid']);
					$id=htmlspecialchars($_POST['auction_id']);
					$dt=new DateTime();
					$current_time=$dt->format("Y-m-d H:i:s");

					if($this->checkInsert($end_time, $last_bid, $bid, $current_time)){
						$this->model->bid($id,$bid,$current_time);
						header("Location:".URL."auctions");
					}
				}
			}
			else{
				$error='Vigane päring, CSRF token ei vasta oodatule';
				$this->message_add($error);
			}
		}
	}


	/**
	 * Kontrollib kasutaja sisestust
	 * @param type $end_time oksjoni lõpuaeg
	 * @param type $last_bid viimane pakkumine
	 * @param type $bid uus pakkumine
	 * @param type $time pakkumise aeg
	 * @return type andmete sobivuse tõeväärtus
	 */
	function checkInsert($end_time, $last_bid, $bid, $time){

		if(strlen($bid)==0){
			$error='Sisestage pakkumine';
			$this->message_add($error);
			return false;
		}
		if($last_bid>=$bid){
			$error='Pakkumine ei saa olla väiksem eelmisest';
			$this->message_add($error);
			return false;

		}
		if($time>=$end_time){
			$error='Lõpuaeg on möödas';
			$this->message_add($error);
			return false;
		}
		return true;
	}


}
?>