<?php

class Bootstrap{
	function __construct(){
		Session::init();

		if (empty($_SESSION['csrf_token'])) {
			$_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(20));
		}

		//Jagab urli kaldkriipsude järgi massiiviks ning eemaldab liigsed kaldkriipsud
		if(isset($_GET['url'])){
			$url=$_GET['url'];
			$url=rtrim($url,'/');
			$url=explode('/',$url);

			//Kui on olemas url-nimeline controller klass, siis luuakse uus controller objekt. Kui klass puudub, luuakse error objekt. Kui on olemas vastava controlleri nimeline model, siis luuakse see controlleri klassi sisse.
			$file='controllers/'.$url[0].'.php';
			if(file_exists($file)){
				require ($file);
				$controller=new $url[0];
				$controller->loadModel($url[0]);

				//Kui url massiivi teisel positsioonil on kirje, siis käivitatakse controller objekti vastav meetod
				if(isset($url[1])){
					if(method_exists($controller, $url[1])){
						
						$controller->{$url[1]}();
					}
					else{
						$this->error();
						exit;
					}
				}
				//Kui url massivi kolmandal positsioonil on kirje, siis näidatakse kasutajale not found lehte.
				if(isset($url[2])){
					$this->error();
					exit;
				}
				//Kasutajale kuvatakse veebilehte
				else{
					$controller->view();
				}
			}
			//Kui url massivi esimesel kohal pole controller klassi nimelist kirjet, siis näidatakse kasutajale not found lehte.
			else{
				$this->error();
				exit;
			}



		}
		//Kui url massiivi esimesel kohal pole midagi, kuvatakse kasutajale avalehte
		else{
			require 'controllers/index.php';
			$controller=new Index;
			$controller->loadModel('index');
			$controller->view();

		}

	}

	function error(){
		require 'controllers/error.php';
		$controller=new Error;
		$controller->view();
		exit;
	}

}

define('URL', 'http://enos.itcollege.ee/~mpaakspu/projekt/index.php?url=');
define('URL2', 'http://enos.itcollege.ee/~mpaakspu/projekt/');


?>