<?php

class View{

	function __construct(){
		$this->active=false;
	}

	//Näitab kasutajale lehekülge
	function render($site){
		require 'views/header.php';
		require 'views/'.$site.'.php';
		require 'views/footer.php';

	}
}

?>