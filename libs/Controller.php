<?php

class Controller{

	//Controller objekti loomisel luuakse ka View objekt ning pannakse paika ajavöönd
	function __construct(){
		date_default_timezone_set('Europe/Tallinn');
		$this->view=new View;
	}

	/**
	 * Kui on olemas controlleri nimeline model, siis luuakse see
	 * @param type $site controller objekti nimi
	 * @return type
	 */
	function loadModel($site){
		$this->model=new Model;
		$file='models/'.$site.'_model.php';
		if(file_exists($file)){
			require $file;
			$name=$site.'_model';
			$this->model=new $name;
		}

	}

	//Kasutaja logitakse välja
	function logoff(){
		
		if (Session::get('login')==false) {
			return false;
		}
    // muuda sessiooni küpsis kehtetuks
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time() - 42000, '/');
		}
    // tühjenda sessiooni massiiv
		$_SESSION = array();
    // lõpeta sessioon
		Session::destroy();
		header('Location: '.URL.'index');
		return true;
		
	}

	// Lisab järjekorda uue sõnumi kasutajale kuvamiseks
	function message_add($message)
	{
		if (empty($_SESSION['messages'])) {
			$_SESSION['messages'] = array();
		}
		$_SESSION['messages'][] = $message;
	}
// Tagastab kõik hetkel ootel olevad sõnumid
	function message_list()
	{
		if (empty($_SESSION['messages'])) {
			return array();
		}
		$messages = $_SESSION['messages'];
		$_SESSION['messages'] = array();
		return $messages;
	}

	// genereerime sessiooni jaoks unikaalse CSRF tokeni
	function check_csrf(){
		return (!empty($_POST['csrf_token']) && $_POST['csrf_token'] == $_SESSION['csrf_token']);
	}
}

?>





