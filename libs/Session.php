<?php

class Session{
	
	//käivitab sessiooni
	public static function init(){
		@session_start();
	}

	//salvestab sessiooni muutuja ja tema väärtuse
	public static function set($key, $value){
		$_SESSION[$key]=$value;
	}

	//tagastab mingi sessiooni muutuja väärtuse
	public static function  get($key){
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}
		else{
			return false;
		}

	}
	//lõpetab sessiooni
	public static function destroy(){
		session_destroy();
	}
}
?>